#include "global.h"
#include <iomanip>
#include <limits>

void config_output(std::ostream& out) {
    out << std::showpoint << std::fixed << std::setprecision(std::numeric_limits<float>::digits10 + 1);
}
