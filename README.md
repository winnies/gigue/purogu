# Projet de Programmation Graphique

Pour lancer l'appli WebGL, aller dans le dossier du même nom et simplement lancer le script "run.sh".
Attention : il faut penser à cloner les sous modules (webgl/js/geometry-processing-js) avec git clone --recursive https://gitlab.com/winnies/gigue/purogu.git
