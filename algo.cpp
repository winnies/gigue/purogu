#include "algo.h"

float clamp(float v, float lo, float hi) {
  assert(!(hi < lo));
  return (v < lo) ? lo : (hi < v) ? hi : v;
}

float clampedAcos(float value) {
  return std::acos(clamp(value, -1.f, 1.f));
}

float angle(const Vector3f& v1, const Vector3f& v2) {
  return clampedAcos(dot(v1.normalized(),v2.normalized()));
}

float signedAngle(const Vector3f& v1, const Vector3f& v2, const Vector3f& normal) {
  return std::atan2(dot(cross(v1,v2), normal), dot(v1,v2));
}
