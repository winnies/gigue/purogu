#include "stats.h"
#include <fstream>
#include <iomanip>
#include <climits>
#include "algo.h"

void export_stats(const MyMesh &mesh)
{
    std::scientific;
    std::ofstream out("stats.txt"); config_output(out);

    out << "NOMBRE DE FACES" << std::endl;
    out << mesh.n_faces() << std::endl;

    out << "NOMBRE D'ARÊTES" << std::endl;
    out << mesh.n_edges() << std::endl;

    out << "NOMBRE DE SOMMETS" << std::endl;
    out << mesh.n_vertices() << std::endl;

    out << "NOMBRE DE FACES ISOLÉES" << std::endl;
    out << std::count_if(mesh.faces_begin(), mesh.faces_end(), [&](FaceHandle face) { return !mesh.cff_iter(face).is_valid(); }) << std::endl;

    out << "NOMBRE D'ARÊTES ISOLÉES" << std::endl;
    out << std::count_if(mesh.edges_begin(), mesh.edges_end(), [&](EdgeHandle edge) {
           return !(mesh.face_handle(mesh.halfedge_handle(edge, 0)).is_valid() || mesh.face_handle(mesh.halfedge_handle(edge, 1)).is_valid());
    }) << std::endl;

    out << "NOMBRE DE SOMMETS ISOLÉS" << std::endl;
    out << std::count_if(mesh.vertices_begin(), mesh.vertices_end(), [&](VertexHandle vertex) { return mesh.valence(vertex) == 0; }) << std::endl;

    out << "BARYCENTRE" << std::endl;
    auto cog = mesh.calc_barycenter();
    out << "[" << cog[0] << ", " << cog[1] << ", " << cog[2] << "]" << std::endl;

    out << "BOITE ENGLOBANTE" << std::endl;
    auto sb = mesh.calc_safe_box(); // A revoir
    out << "Point max : " << "[" << sb.max[0] << "," << sb.max[1] << "," << sb.max[2] << "]" << std::endl;
    out << "Point min : " << "[" << sb.min[0] << "," << sb.min[1] << "," << sb.min[2] << "]" << std::endl;

//    out << "NORMALES AUX FACES" << std::endl;
//    for(MyMesh::FaceIter fi = mesh.faces_begin(); fi != mesh.faces_end(); fi++) {
//        const auto& normal = mesh.normal(*fi);
//        out << "Face " << fi->idx() << " : [" << normal[0] << ", " << normal[1] << ", " << normal[2] << "]." << std::endl;
//    }

//    out << "NORMALES AUX SOMMETS" << std::endl;
//    for(MyMesh::VertexIter vi = mesh.vertices_begin(); vi != mesh.vertices_end(); vi++) {
//        const auto& normal = mesh.normal(*vi);
//        out << "Sommet " << vi->idx() << " : [" << normal[0] << ", " << normal[1] << ", " << normal[2] << "]." << std::endl;
//    }

    out << "AIRE DES FACES" << std::endl;

    double sum = 0.f;
    double min = DBL_MAX;
    double max = DBL_MIN;
    for(FaceHandle face : mesh.faces()){
        auto area = (double)mesh.data(face).area;
        sum += area;
        min = area < min ? area : min;
        max = area > max ? area : max;
    }
    out << "total : " << sum << std::endl;
    out << "min : " << min << " max : " << max << " moyenne : " << sum / mesh.n_faces() << std::endl;
    auto area = [&](FaceHandle face) { return mesh.data(face).area; };
    std::ofstream out2("face_area.csv"); config_output(out);
    auto face_area_freq = calc_freq<float, MyMesh::FaceIter, FaceHandle>(mesh.faces_begin(), mesh.faces_end(), area, min, max, 10);
    face_area_freq.as_csv(out2);


//    auto face_area_stats = calc_list_stats<float, MyMesh::FaceIter, FaceHandle>(mesh.faces_begin(), mesh.faces_end(), area);
//    //    out << "total : " << face_area_stats.sum << std::endl;
//    out << "min : " << face_area_stats.min << " max : " << face_area_stats.max << " moyenne : " << face_area_stats.mean() << std::endl;
//    {
//        std::ofstream out("face_area.csv"); config_output(out);
//        auto face_area_freq = calc_freq<float, MyMesh::FaceIter, FaceHandle>(mesh.faces_begin(), mesh.faces_end(), area, face_area_stats.min, face_area_stats.max, 10);
//        face_area_freq.as_csv(out);
//    }

    out << "VALENCE DES SOMMETS" << std::endl;
    auto valence = [&](VertexHandle vertex) { return mesh.valence(vertex); };
    auto vertex_valence_stats = calc_list_stats<uint, MyMesh::VertexIter, VertexHandle>(mesh.vertices_begin(), mesh.vertices_end(), valence);
    out << "min : " << vertex_valence_stats.min << " max : " << vertex_valence_stats.max << " moyenne : " << vertex_valence_stats.mean() << std::endl;
    {
        std::ofstream out("vertex_valence.csv"); config_output(out);
        auto vertex_valence_freq = calc_freq<uint, MyMesh::VertexIter, VertexHandle>(mesh.vertices_begin(), mesh.vertices_end(), valence);
        vertex_valence_freq.as_csv(out);
    }

    out << "ANGLE DIHÈDRE DES ARÊTES" << std::endl;
//    auto dihedral_angle = [&](EdgeHandle edge) {
//        auto angle = mesh.calc_dihedral_angle(edge);
//        return angle / M_PI * 180.f + (angle >= 0 ? 0 : 360);
//    };
    auto dihedral_angle = [&](EdgeHandle edge) {
        auto angle = mesh.calc_dihedral_angle(edge);
        return angle / M_PI * 180.f;
    };
    auto dihedral_angle_stats = calc_list_stats<float, MyMesh::EdgeIter, EdgeHandle>(mesh.edges_begin(), mesh.edges_end(), dihedral_angle);
    out << "min : " << dihedral_angle_stats.min << " max : " << dihedral_angle_stats.max << " moyenne : " << dihedral_angle_stats.mean() << std::endl;
    {
        std::ofstream out("dihedral_angle.csv"); config_output(out);
        auto dihedral_angle_freq = calc_freq<float, MyMesh::EdgeIter, EdgeHandle>(mesh.edges_begin(), mesh.edges_end(), dihedral_angle, -180.f, 180.f, 36.f);
        dihedral_angle_freq.as_csv(out);
    }
}
