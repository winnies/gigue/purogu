#ifndef STATS_H
#define STATS_H

#include "mesh.h"

void export_stats(const MyMesh& mesh);

#endif // STATS_H
